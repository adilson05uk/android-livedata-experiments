package com.tazkiyatech.experiments.livedata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.master_fragment.*
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate

class MasterFragment : Fragment() {

    private lateinit var timer: Timer

    private val countViewModel
        get() = ViewModelProviders.of(requireActivity()).get(CountViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        countViewModel.countLiveData.observe(this, Observer { count ->
            countTextView.text = getString(R.string.master_fragment_text_format, count)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.master_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()

        timer = Timer()
        timer.scheduleAtFixedRate(1000L, 1000L) { countViewModel.nextCount() }
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }
}
