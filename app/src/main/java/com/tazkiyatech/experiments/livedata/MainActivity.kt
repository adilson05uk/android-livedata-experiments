package com.tazkiyatech.experiments.livedata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    private lateinit var countViewModel: CountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val initialCount = savedInstanceState?.getInt(INITIAL_COUNT_KEY) ?: (0..10).random()

        countViewModel = ViewModelProviders.of(this, CountViewModel.Factory(initialCount))
            .get(CountViewModel::class.java)

        updateInitialCountTextView()

        showMasterFragment()
        showDetailFragment()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(INITIAL_COUNT_KEY, countViewModel.initialCount)
    }

    private fun updateInitialCountTextView() {
        initialCountTextView.text =
            getString(R.string.main_activity_text_format, countViewModel.initialCount)
    }

    private fun showMasterFragment() {
        if (supportFragmentManager.findFragmentById(R.id.masterFragmentContainer) == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.masterFragmentContainer, MasterFragment())
                .commit()
        }
    }

    private fun showDetailFragment() {
        if (supportFragmentManager.findFragmentById(R.id.detailFragmentContainer) == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.detailFragmentContainer, DetailFragment())
                .commit()
        }
    }

    companion object {
        private const val INITIAL_COUNT_KEY = "INITIAL_COUNT_KEY"
    }
}
