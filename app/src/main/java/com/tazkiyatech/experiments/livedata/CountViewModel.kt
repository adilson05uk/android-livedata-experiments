package com.tazkiyatech.experiments.livedata

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CountViewModel(val initialCount: Int) : ViewModel() {

    private val mutableLiveData = MutableLiveData<Int>()

    val countLiveData: LiveData<Int>
        get() = mutableLiveData

    init {
        mutableLiveData.value = initialCount
    }

    fun nextCount() {
        val oldCount = mutableLiveData.value!!
        val newCount = oldCount + 1
        Handler(Looper.getMainLooper()).post { mutableLiveData.value = newCount }
    }

    class Factory(private val initialCount: Int) : ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return CountViewModel(initialCount) as T
        }
    }
}